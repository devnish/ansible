# Complete Grafana Setup with Prometheus and Node Exporter

## Introduction

This repository provides a complete setup for monitoring using Grafana, Prometheus, and Node Exporter. This setup helps you visualize and monitor system metrics and application performance.

## What is Grafana?

**Grafana** is an open-source platform for monitoring and observability. It allows you to visualize data from various sources and create interactive, customizable dashboards. 

- **Working**: Grafana connects to different data sources (like Prometheus) to fetch metrics and display them in dashboards. You can set up alerts based on thresholds to get notified when certain conditions are met.
- **Use Case**: Grafana is used by system administrators and developers to monitor the health and performance of servers, applications, and other infrastructure components.

## What is Prometheus?

**Prometheus** is an open-source systems monitoring and alerting toolkit. It collects and stores metrics as time-series data, recording information with timestamps.

- **Working**: Prometheus scrapes metrics from targets (like Node Exporter) at given intervals, evaluates rule expressions, and triggers alerts if certain conditions are met.
- **Use Case**: Prometheus is used for monitoring and alerting on various metrics, such as CPU usage, memory consumption, and application performance metrics.

## What is Node Exporter?

**Node Exporter** is a Prometheus exporter for hardware and OS metrics exposed by *nix kernels. It provides detailed information about the system’s performance and health.

- **Working**: Node Exporter runs on the target system and exposes metrics such as CPU load, memory usage, and disk I/O. Prometheus then scrapes these metrics to store and visualize in Grafana.
- **Use Case**: Node Exporter is used to monitor the performance and health of individual servers.

## How They Work Together

1. **Node Exporter** runs on each server you want to monitor, exposing metrics about the server’s performance.
2. **Prometheus** scrapes these metrics from Node Exporter at regular intervals and stores them as time-series data.
3. **Grafana** connects to Prometheus and queries this data, providing visualizations and dashboards to monitor the system's health and performance.

## Example Setup
### 1. Run the compose file

```bash
docker-compose up -d
```
**Note: This will setup  and run the compose file for Graffana,Prometheus and Node-Exporter**

### 2. Verifit the compose  file that all 3 are running

```bash
docker-compose ps
```

### 3. Open the Security port 3000 on Azure portal

- Go to Azure server and in network setting create a new inbound port rule
- Open port `3000` for all 

### 4. Login to Graffana dashboards

- `http://serverpublicip:3000`
- This will open graffana dashboards 
- username:`admin`
- password: `admin`
- Enter above creds which will prompt you for new `username` and `password`
- Set the new username and passowrd and `remember` it

### 5. Verify the connection with Node exporter

- go to `connection` tab
- Click on new Connection -
- Search for Prometheus
- add `http://serverlocalip:9091`
- Click on test and Verify to check it

