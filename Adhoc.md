# Ansible Ad-Hoc Commands

## Introduction

Ad-hoc commands in Ansible are quick, one-time tasks that can be executed without writing a playbook. They are useful for simple, straightforward tasks like restarting services, copying files, or gathering information about remote hosts.

## Why Use Ad-Hoc Commands?

- **Quick Execution**: Ideal for simple, one-off tasks that don't require the complexity of a playbook.
- **Flexibility**: Easily execute commands across multiple hosts.
- **No Setup Required**: Run commands directly from the command line without needing to set up a playbook or roles.

## Use Cases

- **System Administration**: Restarting services, checking system status, managing users.
- **File Management**: Copying files, changing permissions.
- **Gathering Facts**: Collecting information about remote hosts (e.g., disk usage, uptime).
- **Installing Packages**: Quickly installing or updating software on multiple machines.

## Advantages

- **Speed**: Execute tasks immediately without writing a playbook.
- **Simplicity**: Simple syntax and command structure.
- **Efficiency**: Perform tasks across multiple hosts simultaneously.

## Disadvantages

- **Not Reusable**: Unlike playbooks, ad-hoc commands are not reusable or maintainable.
- **Limited Functionality**: Suitable for simple tasks only; complex workflows require playbooks.
- **Lack of Documentation**: Ad-hoc commands are not as well-documented as playbooks, making them harder to track.

## Basic Examples

### 1. Ping All Hosts

```bash
ansible all -m ping
```

### 2. Check Disk Usage

```bash
ansible all -m shell -a 'df -h'
```
**Note:** Here all means by default this action will get performed on all the server added in deault inventory file i.e.

- ```/etc/ansible/hosts```
- All groups of server
- If you want to perform on specific group of server using deault inventory file below is the command

```bash
ansible slave_servers -m shell -a 'df -h'
```
**If the Inventory file is at sperate location then**

```bash
ansible slave_servers -i inventory.ini -m shell -a 'df -h'
```

### 3. Installing Nginx on all the server

```bash
ansible all -m apt -a "name=nginx state=present update_cache=yes" --become
```

### Explanation
- ansible all: This targets all hosts defined in the default inventory file (`/etc/ansible/hosts`).
- -m apt: Specifies the Ansible module to use, which is apt for managing packages on Debian-based systems.
- -a "name=nginx state=present update_cache=yes": Specifies the arguments for the apt module:
- name=nginx: The package name to install.
- state=present: Ensures the package is installed.
- update_cache=yes: Updates the package index before installing.
- --become: Uses privilege escalation (sudo) to run the command with root privileges.

### 4. Restart a Service

```bash
ansible all -m service -a 'name=nginx state=restarted' --become
```

### 5. Create a File

```bash
ansible all -m file -a 'path=/tmp/testfile state=touch'
```

### 6. Apt update and upgrade

```bash
ansible slave_servers -m apt -a "update_cache=yes" --become
```

```bash
ansible slave_servers -m apt -a "upgrade=dist" --become
```
