# Ansible Setup on Ubuntu

This guide provides step-by-step instructions to set up Ansible Master and Slave servers on Ubuntu, including installing Ansible, configuring the inventory, running an ad-hoc command, and writing and executing a sample playbook.

## Prerequisites

1. Two Ubuntu servers: **Ansible Master** and **Ansible Slave**.
2. SSH key pair named `ansible.pem` (both servers should have access to this key).

## Step 1: Setup SSH Key

Ensure the `ansible.pem` is accessible to both servers. Copy the key to both servers with:

```bash
scp -i ansible.pem ansible.pem azureuser@ansible-master-ip:/home/user/
scp -i ansible.pem ansible.pem azureuser@ansible-slave-ip:/home/user/
```

Here we push the `ansible.pem` key that we have created on all the server that we have created so that the key is accessible like my myster server is `98.70.110.123` and i created a server with user name as `azureuser`  so my server default location will be `/home/azureuser` and my key is where i am running the command so my command will be:

```bash
scp -i ansible.pem ansible.pem azureuser@98.70.110.123:/home/azureuser/
```

Same way push this ansible.pem key on all the slave server which you have created just by changing the server ip and username if it is different


## Step 2: Install Ansible on the Master Server

### 1. Update System Packages

```bash
sudo apt update && sudo apt upgrade -y
```
### 2. Install Ansible

```bash
sudo apt install software-properties-common -y
```
```bash
sudo add-apt-repository --yes --update ppa:ansible/ansible
```
```bash
sudo apt install ansible -y
```
### 3. Verify Ansible Installation

```bash
ansible --version
```

# Step 3: Test SSH connection from master to slave

```bash
ssh -i /home/user/ansible.key user@ansible-slave-ip
```
**Note**:Here we are testing whether out all the server that we have created whether that are accessible from the master or not 
Eg: Here myslave server ip is `74.225.210.21` and slave-server username is `azureuser`  and my `ansible.pem` is on location `/home/azureuser/`so the that i will run on master will be :

```bash
ssh -i /home/azureuser/ansible.pem azureuser@74.225.210.21
```
same way i need to get it verified for all the slave server if you have created

# Step 4: Create Ansible Inventory File

### 1. Create Inventory File

- Edit the Ansible configuration file to define the inventory.

```bash
sudo vi /etc/ansible/hosts
```

### 2. Add Slave Host to Inventory

- Add the following lines to specify your master and slave:

```bash
[ansible_servers]
ansible-master ansible_ssh_private_key_file=/home/user/ansible.pem
ansible-slave ansible_ssh_private_key_file=/home/user/ansible.pem

[slave_servers]
ansible-slave ansible_ssh_private_key_file=/home/user/ansible.pem
```

**Example**
- master-ip 98.70.110.123
- slave-ip  74.225.210.21
- slave2-ip 74.225.213.235
- and i want perform all action on remote server as azureuser
- So below is the final command

```bash
[ansible_servers]
98.70.110.123 ansible_user=azureuser ansible_ssh_private_key_file=/home/azureuser/ansible.pem
74.225.210.21 ansible_user=azureuser ansible_ssh_private_key_file=/home/azureuser/ansible.pem
74.225.213.235 ansible_user=azureuser ansible_ssh_private_key_file=/home/azureuser/ansible.pem

[slave_servers]
74.225.210.21 ansible_user=azureuser ansible_ssh_private_key_file=/home/azureuser/ansible.pem
74.225.213.235 ansible_user=azureuser ansible_ssh_private_key_file=/home/azureuser/ansible.pem
```

## Step 5: Test Ansible Setup

### 1. Ping Slave Server
- Use the ping module to verify connectivity.

```bash
ansible -i /etc/ansible/hosts slave_servers -m ping
```

- You should see a response from the slave server.

# Congratulation your master slave got configured successfully

 
