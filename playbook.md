# Ansible Playbook

## Introduction

Ansible is a powerful automation tool used for configuration management, application deployment, and task automation. Ansible Playbooks are a way to automate and orchestrate complex IT processes using simple, human-readable YAML configuration files. Playbooks allow you to define a set of tasks that you want to execute on your managed nodes (servers).

## What is an Ansible Playbook?

An Ansible Playbook is a YAML file that contains a series of tasks to be executed on a group of hosts. Each task is an action that Ansible executes, such as installing a package, running a command, or deploying a configuration file. Playbooks provide a more organized, reusable, and scalable way of managing your infrastructure compared to using ad-hoc commands.

## How is it Different from Ad-Hoc Commands?

Ad-hoc commands are single, one-off commands that you run on the command line to perform quick tasks on your managed nodes. They are useful for simple and immediate operations but do not scale well for complex or repetitive tasks.

Key differences between Playbooks and Ad-Hoc Commands:
- **Structure and Organization**: Playbooks are structured and organized in YAML files, allowing for complex and repeatable automation. Ad-hoc commands are one-liners without structured organization.
- **Reusability**: Playbooks can be reused and shared across different projects and teams. Ad-hoc commands are typically one-time operations.
- **Complexity and Flexibility**: Playbooks support conditional logic, loops, handlers, and roles, making them suitable for complex workflows. Ad-hoc commands are limited to simple tasks.

## Advantages of Ansible Playbooks

- **Readability**: Written in YAML, making them easy to read and understand.
- **Reusability**: Playbooks can be reused across different environments and projects.
- **Idempotence**: Ensures that the state of the managed nodes remains consistent after multiple executions.
- **Scalability**: Easily manage large numbers of nodes with a single playbook.
- **Extensibility**: Support for roles and modules allows for modular and extensible automation.

## Disadvantages of Ansible Playbooks

- **Learning Curve**: Requires understanding YAML syntax and Ansible module functions.
- **Performance**: For very large infrastructures, performance can be an issue compared to other tools.
- **Complexity**: Managing very large playbooks can become complex and hard to maintain.

## Sample Ansible Playbook to Install Nginx

Here is a sample Ansible playbook that installs Nginx on a group of servers called `slave_servers` and copies a custom `nginx.conf` file from the master to the slaves.

### Inventory File (inventory.ini)

Make sure you have an inventory file (`inventory.ini`) with the `slave_servers` group defined:

```ini
[slave_servers]
server1 ansible_host=74.225.210.21
server2 ansible_host=74.225.213.235
```


## Playbook (install_nginx.yml)

Create a playbook file named install_nginx.yml
```bash
---
- name: Install and configure Nginx on slave servers
  hosts: slave_servers
  become: yes

  vars:
    nginx_conf_src: /path/to/your/nginx.conf  # Adjust this path

  tasks:
    - name: Install Nginx
      apt:
        name: nginx
        state: present
        update_cache: yes

    - name: Copy custom nginx.conf
      copy:
        src: "{{ nginx_conf_src }}"
        dest: /etc/nginx/nginx.conf
        owner: root
        group: root
        mode: 0644

    - name: Ensure Nginx is running
      service:
        name: nginx
        state: started
        enabled: yes
```
### Explanation
- `name:`: A descriptive name for the playbook task.
- `hosts:`: Specifies the group of hosts (in this case,  `slave_servers`) on which these tasks will be executed.
- `become:`: Enables privilege escalation. yes means tasks will be executed with elevated privileges (sudo).
- `become_user:`: Specifies the user to become after privilege escalation (`root` in this case).

### Tasks
### 1. Install Nginx:

    - Uses the `apt` module to ensure the `nginx` package is installed (`state: present`).
    - `update_cache: yes` updates the local APT package cache before installation.

### 2. Copy Custom Nginx Configuration:

  - Uses the copy module to deploy a custom Nginx configuration file (nginx_conf_src) to `/etc/nginx/nginx.conf` on the remote servers.
  - Sets ownership to `root` and permissions to `0644` for security.


### 3. Ensure Nginx Service is Running:

    - Uses the `service` module to ensure the nginx service is started (`state: started`) and enabled to start on boot (`enabled: yes`)

## Running the Playbook

To run the playbook, use the following command:

```bash
ansible-playbook install_nginx.yml
```
This command tells Ansible to use the inventory file (inventory.ini) and execute the tasks defined in the playbook (install_nginx.yml) on the slave_servers group.

![Working Image](https://gitlab.com/devnish/ansible/raw/main/Screenshot_from_2024-06-23_13-04-59.png)


## Conclusion

Ansible Playbooks provide a powerful way to automate complex IT tasks with simplicity and scalability. By defining tasks in YAML files, you can create reusable, organized, and efficient workflows to manage your infrastructure. This guide covers the basics of Ansible Playbooks, their advantages and disadvantages, and provides a sample playbook to install and configure Nginx on a group of servers.

