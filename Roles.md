## Ansible Roles

### What is an Ansible Role?

An Ansible role is a way to organize and package automation content in a reusable format. It encapsulates tasks, variables, handlers, templates, and other supporting files into a predefined directory structure. Roles are designed to promote reusability, modularity, and maintainability in Ansible automation.

### How is it Used?

Roles are used to:

- **Organize Tasks**: Group related tasks, variables, and handlers together.
- **Promote Reusability**: Roles can be shared across different playbooks and projects.
- **Simplify Maintenance**: Encapsulate complex configurations into manageable units.

### Comparison with ad-hoc Command and Playbook

- **Ad-hoc Commands**: Quick one-liners used for tasks like gathering information (`ansible -m setup all`) or executing simple tasks (`ansible -m command -a "..." all`). They are not suitable for complex, multi-step configurations and lack reusability.

- **Playbooks**: YAML files that define a sequence of tasks to be executed on a set of hosts. Playbooks offer more structure and flexibility compared to ad-hoc commands but may become lengthy and complex for large projects.

### Scenario for Using Ansible Roles

**Scenario**: Managing the configuration of web servers (Nginx) across multiple environments (dev, test, prod).

**Advantages of Using Roles**:

- **Modularity**: Roles help organize tasks and variables related to specific components (e.g., Nginx).
- **Reusability**: Roles can be reused across different playbooks or environments, reducing duplication.
- **Easy Maintenance**: Updates to a role propagate across all playbooks using that role, ensuring consistency.

**Disadvantages**:

- **Learning Curve**: Initial setup and understanding of role structure may require learning.
- **Overhead**: Creating roles for very simple tasks may introduce unnecessary complexity.

### Example and Sample Code

Here’s an example of an Ansible role directory structure and usage:

```yaml
# Directory structure of a typical Ansible role
my_role/
├── defaults/
│   └── main.yml        # Default variables
├── files/              # Files to be copied to hosts
├── handlers/
│   └── main.yml        # Handlers
├── meta/
│   └── main.yml        # Metadata and dependencies
├── tasks/
│   └── main.yml        # Main list of tasks
├── templates/          # Jinja2 templates
└── vars/
    └── main.yml        # Other variables
